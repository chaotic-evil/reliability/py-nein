from fractions import Fraction

from attr.exceptions import FrozenInstanceError
from hypothesis import given
from pytest import raises

from nein import Probability as P
from .utils import percentiles, probabilities


@given(f=percentiles(), p=probabilities())
def test_probability_immutable(f: Fraction, p: P):
    with raises(FrozenInstanceError):
        p.marginal = f  # type: ignore
