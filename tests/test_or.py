from math import isclose

from hypothesis import given

from nein import Probability as P
from .utils import nines, probabilities


@given(p=probabilities())
def test_xor_zero(p: P):
    assert p ^ 0 == P.from_percentile(0)


@given(p=probabilities())
def test_xor_one(p: P):
    assert p ^ 1 == p


@given(p=probabilities())
def test_xor_two(p: P):
    assert p ^ 2 == p | p


@given(p=probabilities(), e=nines())
def test_repeated_or(p: P, e: int):
    q = P.from_percentile(0)
    for _ in range(e):
        q |= p

    assert q == p ^ e


@given(p=probabilities(), q=probabilities())
def test_or_increasing(p: P, q: P):
    assert (p | q).percentile() >= max(p.percentile(), q.percentile())


@given(p=probabilities(), q=probabilities())
def test_or_nines(p: P, q: P):
    assert isclose((p | q).nines(), p.nines() + q.nines(), abs_tol=1e-16)


@given(p=probabilities(), e=nines())
def test_xor_nines(p: P, e: int):
    """(p ^ e).nines() ≃ e * p.nines()

    The computation may fail when the value is too large for math.log10."""

    try:
        assert isclose((p ^ e).nines(), e * p.nines(), rel_tol=2e-5)

    except ValueError as exn:
        assert exn.args == ('math domain error',)
        assert e * p.nines() >= 324
