from fractions import Fraction

from hypothesis import given, reject, strategies as st
from pytest import raises

from nein import Probability as P


@given(f=st.fractions())
def test_probability_validator(f: Fraction):
    if 0 <= f <= 1:
        reject()

    with raises(ValueError):
        P(f)
