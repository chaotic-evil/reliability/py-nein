from hypothesis import given

from nein import Probability as P
from .utils import cmp, nines, PERCENTILES, percentiles


@given(p=percentiles(), q=percentiles())
def test_nines_increasing(p: float, q: float):
    assert cmp(p, q) == cmp(P.from_percentile(p).nines(), P.from_percentile(q).nines())


@given(n=nines())
def test_nine_invert(n: int):
    assert n == P.from_nines(n).nines()


def test_nines_unit():
    for (expected, p) in PERCENTILES:
        assert P.from_percentile(p).nines() == expected
