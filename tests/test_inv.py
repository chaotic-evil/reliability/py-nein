from hypothesis import given

from nein import Probability as P
from .utils import probabilities


@given(p=probabilities())
def test_not_involutive(p: P):
    assert ~ ~ p == p


@given(p=probabilities())
def test_not_percentile(p: P):
    assert (~ p).percentile() == 1 - p.percentile()
