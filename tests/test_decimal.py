from decimal import Decimal

from nein import Probability as P
from .utils import PERCENTILES


def test_decimal():
    for (nine, expected) in PERCENTILES:
        assert P.from_nines(nine).to_decimal() == Decimal(expected)
