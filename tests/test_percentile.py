from fractions import Fraction

from hypothesis import given

from nein import Probability as P
from .utils import cmp, nines, PERCENTILES, percentiles


@given(n=nines(), m=nines())
def test_percentiles_increasing(n: int, m: int):
    assert cmp(n, m) == cmp(P.from_nines(n).percentile(), P.from_nines(m).percentile())


@given(p=percentiles())
def test_percentile_invert(p: Fraction):
    assert P.from_percentile(p).percentile() == p


def test_percentile_unit():
    for (nine, expected) in PERCENTILES:
        assert P.from_nines(nine).percentile() == Fraction(expected)
