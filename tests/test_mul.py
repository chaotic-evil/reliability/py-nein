from hypothesis import given

from nein import Probability as P
from .utils import nines, probabilities


@given(p=probabilities())
def test_power_one(p: P):
    assert p ** 1 == p


@given(p=probabilities())
def test_power_two(p: P):
    assert p ** 2 == p * p


@given(p=probabilities(), q=probabilities())
def test_multiplication_decreasing(p: P, q: P):
    assert (p * q).percentile() <= min(p.percentile(), q.percentile())


@given(p=probabilities(), e=nines())
def test_repeated_mul(p: P, e: int):
    q = P.from_percentile(1)
    for _ in range(e):
        q *= p

    assert q == p ** e
