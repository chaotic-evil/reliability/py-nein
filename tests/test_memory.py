import sys
from fractions import Fraction

import pytest
from hypothesis import given

from nein import Probability as P
from .utils import percentiles, probabilities


class DummyP:
    """A naïve representation of probabilities."""

    marginal: Fraction

    def __init__(self, x):
        self.x = Fraction(x)


@pytest.mark.skipif(sys.implementation.name != 'cpython',
                    reason="PyPy optimises __slots__ automatically.")
@given(x=percentiles())
def test_object_size(x):
    """Check that Probability is 1/3rd smaller than a naïve version."""
    from pympler.asizeof import asizeof as sizeof  # type: ignore

    assert sizeof(P(x)) <= (2 * sizeof(DummyP(x))) / 3


@given(v=probabilities())
def test_weak_ref(v):
    """Check that weak references can be made to Probability objects."""
    import weakref
    assert weakref.ref(v) is not None
