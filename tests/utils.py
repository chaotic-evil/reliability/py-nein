from fractions import Fraction

from hypothesis import strategies as st

from nein import Probability as P


def cmp(x, y):
    return (x > y) - (x < y)


def nines():
    return st.integers(min_value=0, max_value=64)


def percentiles():
    return st.floats(0, 1, exclude_max=True).map(Fraction.from_float)


def probabilities():
    return percentiles().map(P.from_percentile) | nines().map(P.from_nines)


PERCENTILES = [
    ( 0, '0.'),
    ( 1, '0.9'),
    ( 2, '0.99'),
    ( 3, '0.999'),
    ( 4, '0.9999'),
    ( 5, '0.99999'),
    ( 6, '0.999999'),
    ( 7, '0.9999999'),
    ( 8, '0.99999999'),
    ( 9, '0.999999999'),
    (10, '0.9999999999'),
    (11, '0.99999999999'),
    (12, '0.999999999999'),
    (13, '0.9999999999999'),
    (14, '0.99999999999999'),
    (15, '0.999999999999999'),
]
