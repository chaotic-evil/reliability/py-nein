# nein

`nein` is a small, opiniated library for reasoning about probabilities,
typically “nines” of the reliability and responsiveness of various systems.
Its design prioritises correctness, usability, and performance, in that order.


## Usage

Just `pip install nein`, and check out the [reference documentation] for a
detailed description of all methods.

`nein` works with all [currently-supported] versions of Python, and depends
on the wonderful `attrs` library.

`nein` has type annotations, so you can use `mypy` (or any other Python type
checker) to catch mistakes in your uses of its API.

[currently-supported]: https://devguide.python.org/#status-of-python-branches


## Example

Let's say we are investigating a backup system that relies on local,
network-attached storage, replicating its data to the cloud. We assume the
local and cloud components are operating independently (in particular, that
their failure probabilities are independent).

We'd start by loading `nein` and inputting the NAS' parameters:
```python
>>> from nein import Probability as P
>>> local_durability = P.from_nines(5)
>>> local_durability
Probability(1 - 1/100000)
>>> local_availability = P.from_nines(3)
>>> local_availability
Probability(1 - 1/1000)

```

Same thing for the values documented by the cloud provider:
```python
>>> cloud_durability = P.from_nines(15)
>>> cloud_availability = P.from_percentile('0.995')

```

This particular system might be designed such that it is available to writes
when both the cloud provider and local storage are available:
```python
>>> write_availability = local_availability * cloud_availability
>>> write_availability
Probability(1 - 1199/200000)
>>> write_availability.nines()
2.2222108125651325
>>> write_availability.to_decimal()
Decimal('0.994005')

```

Similarly, it might be available for reading when either the cloud provider or
local storage are available:
```python
>>> read_availability = local_availability | cloud_availability
>>> read_availability
Probability(1 - 1/200000)
>>> read_availability.nines()
5.301029995663981
>>> read_availability.to_decimal()
Decimal('0.999995')

```

As we can see, the combined system is less available for writing, but
more available for reads, than either of its parts.

Lastly, backups written to it would endure if they endured in
either backend:
```python
>>> total_durability = local_durability | cloud_durability
>>> total_durability
Probability(1 - 1/100000000000000000000)
>>> total_durability.nines()
20.0
>>> total_durability.to_decimal()
Decimal('0.99999999999999999999')
>>> float(total_durability.percentile())
1.0

```

Not only is the total durability through the roof, but we can see that `nein`
can compute values that Python's `float` type would round to 1. Indeed, each
`Probability` is represented using exact arithmetic, as a [fraction]. (Note that
the choice of internal representation isn't guaranteed, and may change in the
future as long as precision isn't compromised.)

This is necessary, as round “numbers of nines” (like 0.9, 0.99, 0.999, ...)
cannot be exactly represented by binary floating-point numbers of any width,
and the induced error quickly climbs as the values near 1.

[fraction]: https://docs.python.org/3/library/fractions.html
