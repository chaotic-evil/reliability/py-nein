import decimal
from decimal import Decimal
from fractions import Fraction
from math import log10
from typing import Optional, SupportsFloat, Union

import attr

__version__ = "0.1.0-dev"


@attr.s(auto_attribs=True, auto_detect=True, frozen=True, slots=True)
class Probability:
    marginal: Fraction = attr.ib()

    @marginal.validator
    def _validate_proba(self, _attribute, f: Fraction):
        if not (0 <= f <= 1):
            raise ValueError(f"Expected a number between 0 and 1, got {f}")

    @staticmethod
    def from_nines(nines: int) -> 'Probability':
        """Construct a :py:class:`Probability`, given its number of nines.

        :param nines: A non-negative :py:class:`int`.  The returned probability
          is equivalent to ``1 - 10⁻ⁿⁱⁿᵉˢ``.
        """
        if nines < 0:
            raise ValueError(f"Expected a non-negative number, got {nines}.")

        return Probability(Fraction(1, 10 ** nines))

    @staticmethod
    def from_percentile(p: Union[str, Decimal, Fraction, SupportsFloat]) -> 'Probability':
        """Construct a :py:class:`Probability`, given its value.

        :param p: must represent a value between 0 and 1, and can be either:

          - a :py:class:`Decimal`, :py:class:`float`, or :py:class:`Fraction` ;
          - a :py:class:`str`, in a format accepted by the :py:class:`Fraction`
            constructor;
          - a value which can be (potentially lossily) converted to
            :py:class:`float`, then losslessly converted to a :py:class:`Fraction`.

        >>> p = Probability.from_percentile(Fraction(1, 5))
        >>> p.to_decimal()
        Decimal('0.2')

        >>> assert p == Probability.from_percentile(Decimal('0.2'))
        >>> assert p == Probability.from_percentile('1/5')
        >>> assert p == Probability.from_percentile('2e-1')
        >>> assert p == Probability.from_percentile('0.2')
        >>> Probability.from_percentile(0.2).to_decimal()
        Decimal('0.2000000000000000111022302463')

        .. warning::
          As seen above, passing in a :py:class:`float` value, even as a literal,
          can yield an unexpected result.  This is because the literal (here, 0.2)
          cannot be exactly represented as a binary floating-point number, so
          Python interprets it as the closest representable number.
        """
        if isinstance(p, (float, str, Decimal, Fraction)):
            f = Fraction(p)
        else:
            f = Fraction.from_float(p.__float__())

        if not 0 <= f <= 1:
            raise ValueError(f"Expected a number between 0 and 1, "
                             f"got '{p}' {f}")

        return Probability(1 - f)

    def percentile(self) -> Fraction:
        """Get a :py:class:`Fraction` representing this probability.

        This method is essentially the inverse of :py:meth:`from_percentile`.
        """
        return 1 - self.marginal

    def nines(self) -> float:
        """Get a :py:class:`float` representing the “number of nines”.

        .. warning::
          While this method is essentially the inverse of :py:meth:`from_nines`,
          :py:func:`log10` performs rounding, so the result is inexact and cannot
          be round-tripped back to the original value.
        """
        return -log10(self.marginal)

    def to_decimal(self, ctx: Optional[decimal.Context] = None) -> decimal.Decimal:
        """Convert to a :py:class:`Decimal` number.

        :param ctx: An optional :py:class:`decimal.Context` in which the
          computations are performed, allowing users to set precision etc.

        .. warning:: :py:meth:`to_decimal` necessarily performs rounding, so
          its return value is inexact and cannot be round-tripped back to the
          original :py:class:`Probability`.
        """
        with decimal.localcontext(ctx):
            n = Decimal(self.marginal.numerator)
            d = Decimal(self.marginal.denominator)
            return 1 - n / d

    def __float__(self) -> float:
        return float(1 - self.marginal)

    def __mul__(self, rhs: 'Probability') -> 'Probability':
        """Probability that 2 independent events happen conjointly."""
        return Probability(1 - (1 - self.marginal) * (1 - rhs.marginal))

    def __pow__(self, exponent: int) -> 'Probability':
        """Probability that multiple independent instances of an event happen conjointly."""
        return Probability(1 - (1 - self.marginal) ** exponent)

    def __invert__(self) -> 'Probability':
        """Take the negation of an event."""
        return Probability(1 - self.marginal)

    def __or__(self, other: 'Probability') -> 'Probability':
        """Probability that at least one of 2 independent events happen."""
        return Probability(self.marginal * other.marginal)

    def __xor__(self, exponent: int) -> 'Probability':
        """Probability that at least one of multiple independent instances happen."""
        return Probability(self.marginal ** exponent)

    def __repr__(self) -> str:
        m = self.marginal
        return f"Probability(1 - {m.numerator}/{m.denominator})"
